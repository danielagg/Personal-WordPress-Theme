# Personal WordPress Theme

### To see the design live, please visit to my <a href="http://danielagg.com" target="_blank">personal website.</a>

This is the theme I created from scratch, following <a href="https://www.youtube.com/watch?v=8OBfr46Y0cQ&list=PLpcSpRrAaOaqMA4RdhSnnNcaqOVpX7qi5" target="_blank">LearnWebCode's</a> tutorial series on YouTube.

<hr>

### How to install on your WordPress blog
Simple click the 'Download ZIP' button, and upload the Theme (or rename it first to whatever you like) folder to your site's /wp-content/themes folder. After uploading it, on your Dashboard click Appearance --> Themes, and activate the theme. 

### How to assign menus/navigation bars
There are two locations for a menu:
* On the 'total-top', right before the site's name & description is placed (Social Media Menu)
* Next to the site's name & description (Main Navigation Menu)

You can assign a menu to either of these places, by on your WordPress Dashboards' Menu page, Menu Settings part, by checking the desired Theme location.

###### If you don't have a menu assigned to either of these positions, nothing will be displayed there.