<?php if(have_comments() ) : ?>
	<!--<h1 class="comments">
		<?php comments_number('No Comments', '1 Comment', '% Comments');?>
	</h1>
	<hr>-->
	<ul class="comment-list">
		<?php wp_list_comments('callback=custom_comments'); ?>
	</ul>
	<hr style="height:1px; background-color:#ccc; border:none;">
	
<?php endif; ?>

<?php
	$comments_args = array(
		'label_submit' => 'Send',
		'title_reply' => 'Post a Comment',
		'comment_notes_after' => ''
	);
	comment_form($comments_args);
?>