<?php


// Reply comments part
if(get_option('thread_comments'))
{
	wp_enqueue_script('comment-reply');
}

// Include styling
function mysite_resources() {
	wp_enqueue_style('style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'mysite_resources');


// Navigation menus
add_action('init', 'custom_menus');
function custom_menus() {
   register_nav_menus(
        array(
            'primary-menu' => __( 'Main Navigation Menu' ),
            'secondary-menu' => __( 'Social Media Menu' )
        )
    );
}

// Comment section
function custom_comments($comment, $args, $depth) {
	$GLOBALS[' comment '] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
    	<div id="comment-<?php comment_ID(); ?>">

            <div class="commentmetadata">
                <!--<a href="<?php echo htmlspecialchars( get_comment_link($comment->comment_ID )) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time() ) ?></a>-->
                <p><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time() ) ?></p>
            </div> 

        	<div class="comment-author-vcard">
             	<?php echo get_avatar($comment, $size='48', $default='<path_to_url>' ); ?>
              
            	<?php printf (__('<span class="comment_poster_name">%s</span>  <span class="says"> commented:</span>'), get_comment_author_link()) ?>
    		</div>
          
        	<?php if ($comment->comment_approved =='0') : ?>
        		<em><?php _e('Your comment is awaiting moderation.') ?> </em>
        		<br />
        	<?php endif ; ?>
          
        	<div class="comment-text">       
                <p class="edit-comment"><?php edit_comment_link(__('Edit comment'), '   ', ' ') ?></p>
                <?php comment_text() ?>
            </div>
          
        	<div class="reply">
            	<p><span><?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth'] ))) ?></span></p>
              
        	</div>
        </div>
    <?php
}﻿
?>