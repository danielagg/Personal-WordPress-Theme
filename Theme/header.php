<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width">
	<title>Daniel Agg</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>

	<!-- If wordpress wants to add extra stuff to the header -->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="soc-media">
		<?php
			if (has_nav_menu('secondary-menu')) {
			    wp_nav_menu (array('theme_location' => 'secondary-menu','menu_class' => 'nav'));
			}
		?>
	</div>
	<div class="container">
		<header class="site-header">
			<h1><a class="logo" href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>
			<h2 class="site-desc"><span><?php bloginfo('description'); ?></span></h2>

			<nav ul class="site-nav">
				<?php
					if (has_nav_menu('primary-menu')) {
				    	wp_nav_menu (array('theme_location' => 'primary-menu','menu_class' => 'nav'));
					}
				?> 
			</nav>
		</header>
	</div>
	<div class="cleartop"></div>
	<div class="container">