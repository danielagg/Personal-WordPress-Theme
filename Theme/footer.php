<?php wp_footer(); ?>
	</div><!-- End of container -->

	<footer>
		<p>&copy; <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>, <?php echo date('Y'); ?>.</p>
	</footer>

</body>
</html>