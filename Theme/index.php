<?php

	get_header();

	if(have_posts()) :
		while(have_posts()) : the_post(); ?>
			<article class="post">
				<h1><a href="<?php the_permalink(); ?>"><span><?php the_title(); ?></span></a></h1>
				<p class="post-info-page">Posted on <?php the_date();?>, in the <span class="post-category"><?php the_category( ' ' ); ?></span> category.</p>
				<!--<p><?php the_content('Continue reading →'); ?></p>-->
				<p><?php the_content("<span class='custom-more'>Continue reading →</span>"); ?></p>

			</article>
		<?php endwhile;

		?><div style="width:100%; height:1px; margin-top:-10px!important; display:block;"></div><?php


		if( get_previous_posts_link() ) :
			?><span style="display:inline-block; float:right; margin:-30px 0 10px 0!important;"><?php previous_posts_link( 'Newer Entries →' );?></span><?php
		endif;

		if( get_next_posts_link() ) :
			next_posts_link( '← Older Entries', 0 );
		endif;



		

	else:
		echo"<p class='nothing-found'>No content to display yet.</p>";
	endif;

	get_footer();
?>