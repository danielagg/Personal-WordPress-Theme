<?php

	get_header();

	if(have_posts()) :
		while(have_posts()) : the_post(); ?>
			<article class="post">
				<!--<h2><span><?php the_title(); ?></span></h2>-->
				<p><?php the_content(); ?></p>
			</article>
		<?php endwhile;
	else:
		echo"<p class='nothing-found'>No content to display yet.</p>";
	endif;

	get_footer();
?>